package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

public class RobotControlActivity extends AppCompatActivity {
    TextView flash;
    Button rightButton;
    Button downButton;
    Button leftButton;
    Button upButton;
    Button BrightnessButton,BTButton;


    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robotcontrol);

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }


        //flash = findViewById(R.id.textView);
        rightButton = findViewById(R.id.rightButton);
        downButton = findViewById(R.id.downButton);
        leftButton = findViewById(R.id.leftButton);
        upButton = findViewById(R.id.upButton);
        BTButton=findViewById(R.id.BTButton);
        BTButton.setOnClickListener(view ->{
            Intent intent=new Intent(RobotControlActivity.this, Bluetooth.class);
            startActivity(intent);
        });
        BrightnessButton = findViewById(R.id.BrightnessButton);

        BrightnessButton.setOnClickListener(view -> {
            Intent intent1 = new Intent(RobotControlActivity.this, ParametersActivity.class);
            startActivity(intent1);

        });




    }

    @Override
    public void onBackPressed(){
        ToggleButton toggleButton;
        finish();
    }

}