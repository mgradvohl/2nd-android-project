package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.widget.ImageView;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ImageView splash;
    Handler handler;
    TextView flash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //splash = findViewById(R.id.splash);

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        //flash = findViewById(R.id.textView);
        handler =new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(MainActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        },3000);

    }
}