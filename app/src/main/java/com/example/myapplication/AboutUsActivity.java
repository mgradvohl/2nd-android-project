package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutUsActivity extends AppCompatActivity {

    ImageView car1;
    ImageView car2;
    ImageView car3;
    TextView flash;
    TextView t1;
    TextView t2;
    TextView t3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        car1 = findViewById(R.id.firstMemberImageView);
        car2 = findViewById(R.id.secondMemberImageVIew);
        car3 = findViewById(R.id.thirdMemberImageView);
        t3 = findViewById(R.id.secondMemberTextView);
        t1 = findViewById(R.id.firstMemberTextView);
        t2 = findViewById(R.id.thirdMemberTextView);
        //flash = findViewById(R.id.textView);

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }
    }

    @Override
    public void onBackPressed(){
        finish();
    }
}