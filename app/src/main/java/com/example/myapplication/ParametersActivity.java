package com.example.myapplication;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ParametersActivity extends AppCompatActivity {
    // activity objects
    private SeekBar brightnessSeekBar;
    private TextView lightLevel;

    private static Context context;
    private SensorManager sensorManager;
    private ContentResolver cResolver;// Content resolver used as a handle to the system's settings
    private Window window;// window object that will store a reference to the current window
    CheckBox brightnessCheckBox;
    // Variable to store brightness value
    private int brightness;


    private WindowManager.LayoutParams layoutParams;


    /**Called when the activity is first created.*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parameters);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        // hide native UI bar
        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        // UI components
        brightnessSeekBar  = findViewById(R.id.brightnessSeekBar);
        brightnessCheckBox = findViewById(R.id.brightnessCheckBox);

        lightLevel=findViewById(R.id.lightLevel);

        cResolver=getContentResolver(); // Get the  content Resolver
        window=getWindow(); // Get the current window

        // -------------------------- seekbar settings --------------------------

        //Set the  seekbar range between 0 and 255
        brightnessSeekBar.setMax(255);
        //set the seekbar progress to 1
        brightnessSeekBar.setKeyProgressIncrement(1);
        layoutParams = getWindow().getAttributes();

        //Get the brightness of the current system
        brightness= Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS,0);

        //Set the progress of the seek bar based on the system's brightness
        brightnessSeekBar.setProgress(brightness);

        brightnessCheckBox.setOnClickListener(view -> {
            if(brightnessCheckBox.isChecked()){
                brightnessSeekBar.setEnabled(false);
            } else {
                brightnessSeekBar.setEnabled(true);
                brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
                {
                    public void onStopTrackingTouch(SeekBar seekBar){}      //Nothing handled here
                    public void onStartTrackingTouch(SeekBar seekBar){}     //Nothing handled here
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                    {
                        //Set the minimal brightness level
                        //if seek bar is 20 or any value below
                        boolean canWrite=Settings.System.canWrite(ParametersActivity.this);
                        if(canWrite){
                            Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
                            Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,progress);
                        } else{
                            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                            startActivity(intent);
                        }
                        if(progress<=20)
                        {
                            brightness=20; //Set the brightness to 20
                        }
                        else //brightness is greater than 20
                        {
                            brightness = progress; //Set brightness variable based on the progress bar
                        }
                    }
                });
            }
        });

    }

    private SensorEventListener listener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            // The value of the first subscript in the values array is the current light intensity
            float value = event.values[0];
            lightLevel.setText("Current light level is " + value + " lx");
            if (brightnessCheckBox.isChecked()){
                if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                    float lightLevel = event.values[0];

                    if (lightLevel < 10) {
                        layoutParams.screenBrightness = 0.1f;
                    } else if (lightLevel < 1000) {
                        layoutParams.screenBrightness = 0.5f;
                    } else {
                        layoutParams.screenBrightness = 1.0f;
                    }

                    getWindow().setAttributes(layoutParams);
                }


            }
            if(!brightnessCheckBox.isChecked()){
                brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
                {
                    public void onStopTrackingTouch(SeekBar seekBar){}      //Nothing handled here
                    public void onStartTrackingTouch(SeekBar seekBar){}     //Nothing handled here
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                    {
                        //Set the minimal brightness level
                        //if seek bar is 20 or any value below
                        boolean canWrite=Settings.System.canWrite(ParametersActivity.this);
                        if(canWrite){
                            Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
                            Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,progress);
                        } else{
                            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                            startActivity(intent);
                        }
                        if(progress<=20)
                        {
                            brightness=20; //Set the brightness to 20
                        }
                        else //brightness is greater than 20
                        {
                            brightness = progress; //Set brightness variable based on the progress bar
                        }
                    }
                });
            }
        }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };








    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (sensorManager != null) {
            sensorManager.unregisterListener(listener);
        }

    }

}