package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


public class MenuActivity extends AppCompatActivity {
    Button commandButton;
    Button sensorsButton;
    Button aboutUsButton;
    TextView flash;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        //flash = findViewById(R.id.textView);
        commandButton = findViewById(R.id.commandButton);
        sensorsButton = findViewById(R.id.sensorsButton);
        aboutUsButton = findViewById(R.id.aboutUsButton);

        commandButton.setOnClickListener(view -> {
            Intent intent1 = new Intent(MenuActivity.this, RobotControlActivity.class);
            startActivity(intent1);

        });

        sensorsButton.setOnClickListener(view -> {
            Intent intent2 = new Intent(MenuActivity.this, CheckSensorsActivity.class);
            startActivity(intent2);

        });

        aboutUsButton.setOnClickListener(view -> {
            Intent intent3 = new Intent(MenuActivity.this, AboutUsActivity.class);
            startActivity(intent3);

        });
    }

}