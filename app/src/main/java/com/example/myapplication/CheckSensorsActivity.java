package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

public class CheckSensorsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_sensors);

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors  = sensorManager.getSensorList(Sensor.TYPE_ALL);
        ListView list= findViewById(R.id.list);
        list.setAdapter(new SensorsAdapter(this, R.layout.row_item, sensors));
    }
}